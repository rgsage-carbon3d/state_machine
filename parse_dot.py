#
# From 'dot' language file generate scaffold python source state_machine with annotations like:
#  ~state:scheduled {"color": "lightblue", "style": "filled"}
#  ~transition:end {"label": "printing now"}
# 
#
import sys
import pydot
import pyparsing
import json

# file in 'dot' language format
input = sys.argv[1]

#g = pydot.graph_from_dot_file('updater_states.dot')
g = pydot.graph_from_dot_file(input)

# { state (str): attributes (dict) }
nodes={}
# { from_state (str): [to_state (str), attributes (dict)] }
edges={}


def get_attr(d):
    attr = d.get_attributes()
    label = attr.get('label')
    if label:
        attr['label'] = label.strip('"')
    return attr

for n in g.get_nodes():
    nodes[n.get_name()] = get_attr(n)
    #nodes[n.get_name()] = n.get_attributes()


for e in g.get_edges():
    s = e.get_source()
    d = e.get_destination()
    n_edges = edges.get(s)

    if not n_edges:
        n_edges = []
        edges[s] = n_edges
    if not nodes.get(s):
        nodes[s] = None
    if isinstance(d, dict):
        for k in d['nodes'].keys():
            n_edges.append((k,None))
    else:
        if not nodes.get(d):
            nodes[d] = None
        #n_edges.append((d,e.get_attributes()))
        n_edges.append((d,get_attr(e)))

# find the set of nodes that have no incoming edges (the 'begin' states)
# by collecting the destitnation states and subtracting those from 'all' states
dests=set([d[0] for sub in edges.values() for d in sub])
begin=set(nodes.keys()).difference(dests)


header = """
class StateMachine:

    def run(self, state):
        while state:
            transition = state.run()
            print transition
            state = transition

class State:
    def __init__(self):
        self.already_done = set()

    def condition(self, dest):
        if dest in self.already_done:
            return False
        self.already_done.add(dest)
        return True
"""

state_class = """
# ~state:{state} {attr}
class {state}(State):
    
    def run(self):
        #work here
"""

transition = """
        if self.condition('{dest}'):
            # ~transition:{dest} {attr}
            return {dest}
"""

def format_attr(attr):
    if attr:
        #return '[' + ', '.join(['='.join(tuple) for tuple in attr.items()]) + ']'
        return json.dumps(attr)
    return ''

print header
for n,attr in nodes.items():
    print state_class.format(state=n, attr=format_attr(attr))
    n_edges = edges.get(n)
    if n_edges:
        for dest, attr in n_edges:
            print transition.format(dest=dest, attr=format_attr(attr))
    else:
        print """
        return None
"""
for n in nodes:
    print "{0}={0}()".format(n)

#print begin
b = begin.pop()
print 'StateMachine().run({})'.format(b)


"""
print
print

for n,attr in nodes.items():
    print n, format_attr(attr)
    n_edges = edges.get(n)
    if n_edges:
        for dest, attr in n_edges:
            print '  ->', dest, format_attr(attr)
"""
