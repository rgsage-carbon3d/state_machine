#!/usr/bin/python
#
# Generate .png state machine diagram from python source file with annotations
#  ~state:scheduled {"color": "lightblue", "style": "filled"}
#  ~transition:end {"label": "printing now"}
#
import sys
import re
import json
from graphviz import Digraph

STATE_REGEX = re.compile('\s*#\s*~state:(?!->)\s*(\S+)\s+(.*)')
TRANSITION_REGEX = re.compile('\s*#\s*~transition:(\S+)\s+(.*)')

def main():
    # state_machine.py
    input = sys.argv[1]

    states = []
    edges = []
    with open(input) as fp:
        for line in fp:
            m_s = STATE_REGEX.match(line)
            if m_s:
                state = m_s.group(1)
                attr = m_s.group(2)
                states.append((state,attr))
            else:
                m_t = TRANSITION_REGEX.match(line)
                if m_t:
                    dest = m_t.group(1)
                    attr = m_t.group(2)
                    edges.append((state, dest, attr))


    g = Digraph(format='png')
    for s in states:
        state = s[0]
        attr_json = s[1]
        if attr_json:
            try:
                attr = json.loads(attr_json)
            except ValueError:
                print "Invalid json: {}".format(attr_json)
                sys.exit(1)
            g.node(state, **attr)
        else:
            g.node(state)

    for e in edges:
        source = e[0]
        dest = e[1]
        attr_json = e[2]
        if attr_json:
            try:
                attr = json.loads(attr_json)
            except ValueError:
                print "Invalid json: {}".format(attr_json)
                sys.exit(1)
            g.edge(source, dest, **attr)
        else:
            g.edge(source, dest)


    g.render(input + '.dot')

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print "Usage:"
        print "  {} [annotated-input-file]".format(sys.argv[0])
    else:
        main()
