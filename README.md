# README #
This is POC only. For real use needs a bit more work...

From 'dot' language file generate scaffold python state machine source with annotations that enable .png generation of latest iteration

### First install some stuff ###
TODO not 100% this are completely correct. Need to confirm:

* apt-get install graphviz (?) 
* pip install graphviz (for render_sm.py: python source -> .png)
* pip install pydot (for parse_dot.py: .dot -> python source) 
* pip install pyparsing (for parse_dot.py: .dot -> python source)
* ?

Note some folks recommend instead python networkx module for these tasks but I could not get installation working...



### Usage ###

Generate state machine initial source file like:
```
python parse_dot.py updater_states.dot > ~/my_sm.py
```

Generate visual .png like:
```
python render_sm.py ~/my_sm.py
```
Creates: 

* ~/my_sm.py.dot (.dot language version)
* ~/my_sm.py.dot.png (drawing for view)

Generate visual .png directly form the .dot file like:
```
dot -Tpng -O updater_states.dot
```
Creates:

* updater_states.dot.png



Annotations like:
```
#!python

# ~state:scheduled {"color": "lightblue", "style": "filled"}
# ~transition:end {"label": "printing now"}

```

### Links ###
http://www.graphviz.org/Documentation/dotguide.pdf
http://www.tonyballantyne.com/graphs.html